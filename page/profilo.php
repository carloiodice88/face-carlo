<?php
// inizializza sessione
session_start();

//  Verifica se l'utenet è gia loggato altrimenti lo rimanda al login
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: ./login.php");
    exit;
}

// Include file config db
require_once "../components/config.php";

// Definisci le variabili e inizializza con valori vuoti
$new_password = $confirm_password = "";
$new_password_err = $confirm_password_err = "";

// Elaborazione dei dati del modulo quando il modulo viene inviato
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    // Convalida la nuova password
    if (empty(trim($_POST["new_password"]))) {
        $new_password_err = "Si prega di inserire la nuova password";
    } elseif (strlen(trim($_POST["new_password"])) < 6) {
        $new_password_err = "La password deve contenere almeno 6 caratteri.";
    } else {
        $new_password = trim($_POST["new_password"]);
    }

    // Convalida la conferma password
    if (empty(trim($_POST["confirm_password"]))) {
        $confirm_password_err = "Si prega di confermare la password.";
    } else {
        $confirm_password = trim($_POST["confirm_password"]);
        if (empty($new_password_err) && ($new_password != $confirm_password)) {
            $confirm_password_err = "La password non corrisponde.";
        }
    }

    // Verificare gli errori di input prima di aggiornare il database
    if (empty($new_password_err) && empty($confirm_password_err)) {
        
        $sql = "UPDATE users SET password = ? WHERE id = ?";

        if ($stmt = mysqli_prepare($link, $sql)) {
            //  Associa le variabili all'istruzione preparata come parametri
            mysqli_stmt_bind_param($stmt, "si", $param_password, $param_id);

            // imposta parametri
            $param_password = password_hash($new_password, PASSWORD_DEFAULT);
            $param_id = $_SESSION["id"];

            // Tentativo di eseguire l'istruzione preparata
            if (mysqli_stmt_execute($stmt)) {
                // Password aggiornata con successo. Distruggi la sessione e reindirizza alla pagina di accesso
                session_destroy();
                header("location: /auth/login.php");
                exit();
            } else {
                echo "Ops! Qualcosa è andato storto. Per favore riprova più tardi.                ";
            }

            // chiudi dichiarazione
            mysqli_stmt_close($stmt);
        }
    }

    // chiudi connessione
    mysqli_close($link);
}
?>

<?php $page='profilo'; include '../components/header-dashboard.php' ?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">PROFILO</h4>
                    </div>
                    <div class="card-body">
                        <form>
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" class="form-control" disabled="" value="Admin">
                                    </div>
                                </div>
                                <div class="col-md-6 pl-1">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email address</label>
                                        <input type="email" class="form-control" disabled="" value="administration@rilevamentofacciale.it">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label>Nome</label>
                                        <input type="text" class="form-control" disabled="" placeholder="" value="Carlo">
                                    </div>
                                </div>
                                <div class="col-md-6 pl-1">
                                    <div class="form-group">
                                        <label>Cognome</label>
                                        <input type="text" class="form-control" disabled="" placeholder="" value="Iodice">
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">CAMBIA PASSWORD</h4>
                    </div>
                    <div class="card-body">
                        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                            <div class="row">
                                <div class="col-md-5 px-50">
                                    <div class="form-group">
                                        <label>NUOVA PASSWORD</label>
                                        <input type="password" name="new_password" class="form-control" placeholder="" value="" REQUEST <?php echo (!empty($new_password_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $new_password; ?>">
                                        <span class="invalid-feedback"><?php echo $new_password_err; ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 px-50">
                                    <div class="form-group">
                                        <label>RIPETI PASSWORD</label>
                                        <input type="password" name="confirm_password" class="form-control" placeholder="" value="" REQUEST<?php echo (!empty($confirm_password_err)) ? 'is-invalid' : ''; ?>">
                                        <span class="invalid-feedback"><?php echo $confirm_password_err; ?></span>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-info btn-fill pull-right">AGGIORNA PASSWORD</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include '../components/footer-dashboard.php' ?>