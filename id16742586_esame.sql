-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Creato il: Mag 19, 2021 alle 10:14
-- Versione del server: 10.3.16-MariaDB
-- Versione PHP: 7.3.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id16742586_esame`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `anagrafia`
--

CREATE TABLE `anagrafia` (
  `id_anagrafia` int(11) NOT NULL,
  `nome_anagrafia` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `cognome_anagrafia` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `anagrafia`
--

INSERT INTO `anagrafia` (`id_anagrafia`, `nome_anagrafia`, `cognome_anagrafia`) VALUES
(1, 'Davide', 'Palladino');

-- --------------------------------------------------------

--
-- Struttura della tabella `timestamp`
--

CREATE TABLE `timestamp` (
  `id_face` int(11) NOT NULL,
  `nome` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `cognome` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `data` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `created_at`) VALUES
(1, 'carlo', '$2y$10$/XLQWJllwzNfChqxSRXjnesVhni.Y691kAMyzi5QbzEpF4lsRM/9a', '2021-05-03 19:44:52'),
(3, 'admin', '$2y$10$8OYeszAwRWpKAXk4wRkGpuQmVbZma1DA2.og0k5m0L9HP8qO0bV2u', '2021-05-13 22:49:27');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `anagrafia`
--
ALTER TABLE `anagrafia`
  ADD PRIMARY KEY (`id_anagrafia`);

--
-- Indici per le tabelle `timestamp`
--
ALTER TABLE `timestamp`
  ADD PRIMARY KEY (`id_face`);

--
-- Indici per le tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `anagrafia`
--
ALTER TABLE `anagrafia`
  MODIFY `id_anagrafia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT per la tabella `timestamp`
--
ALTER TABLE `timestamp`
  MODIFY `id_face` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT per la tabella `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
