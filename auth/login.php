<?php
// inizializza sessione
session_start();

//  Verifica se l'utenet è gia loggato altrimenti lo rimanda al login
if (isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true) {
    header("location: ./welcome.php");
    exit;
}

// includi file config db
require_once "../components/config.php";

// Definisci le variabili e inizializza con valori vuoti
$username = $password = "";
$username_err = $password_err = $login_err = "";

// Elaborazione dei dati del modulo quando il modulo viene inviato
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    // Verifica se il campo username è riempito
    if (empty(trim($_POST["username"]))) {
        $username_err = "Si prega di inserire il nome utente.";
    } else {
        $username = trim($_POST["username"]);
    }

    // Verifica se il campo password è riempito
    if (empty(trim($_POST["password"]))) {
        $password_err = "Per favore inserisci la tua password";
    } else {
        $password = trim($_POST["password"]);
    }

    // Verifica credenziali
    if (empty($username_err) && empty($password_err)) {
        
        $sql = "SELECT id, username, password FROM users WHERE username = ?";

        if ($stmt = mysqli_prepare($link, $sql)) {
            //  Associa le variabili all'istruzione preparata come parametri
            mysqli_stmt_bind_param($stmt, "s", $param_username);

            // imposta parametri
            $param_username = $username;

            // Tentativo di eseguire l'istruzione preparata
            if (mysqli_stmt_execute($stmt)) {
                // risultato
                mysqli_stmt_store_result($stmt);

                // Verifca se l'username esiste già, se si verifica la password  
                if (mysqli_stmt_num_rows($stmt) == 1) {
                    
                    mysqli_stmt_bind_result($stmt, $id, $username, $hashed_password);
                    if (mysqli_stmt_fetch($stmt)) {
                        if (password_verify($password, $hashed_password)) {
                            // Password è corretta, iniza una nuova sessione
                            session_start();

                            // sessione variabili
                            $_SESSION["loggedin"] = true;
                            $_SESSION["id"] = $id;
                            $_SESSION["username"] = $username;

                            // reinderizza alla dashboard
                            header("location: ./welcome.php");
                        } else {
                            // passworsd non valida ,messaggio di errore
                            $login_err = "Username o password non validi.";
                        }
                    }
                } else {
                    // username non esiste ,messaggio di errore
                    $login_err = "Username o password non validi.";
                }
            } else {
                echo "Ops! Qualcosa è andato storto. Per favore riprova più tardi.";
            }

            // chiudi
            mysqli_stmt_close($stmt);
        }
    }

    // chiudi connessione
    mysqli_close($link);
}
?>

<?php include "../components/header-main.php" ?>
<link rel="stylesheet" href="../style.scss">



<body>

    <?php
    if (!empty($login_err)) {
        echo '<div class="alert alert-danger">' . $login_err . '</div>';
    }
    ?>

    <div class="container">

        <form class="center" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="border-gradint-input">
                <input class="login-data" name="username" type="text" placeholder="Username <?php echo (!empty($username_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $username; ?>"">
                <span class="invalid-feedback"><?php echo $username_err; ?></span>
            </div>
            <div class="border-gradint-input">
                <input class="login-data" name="password" type="password" placeholder="Password" <?php echo (!empty($password_err)) ? 'is-invalid' : ''; ?>">
                <span class="invalid-feedback"><?php echo $password_err; ?></span>
            </div>
            <a class="password-recovery" href="">Password Dimenticata?</a>
            <button class="btn-login"> <a class="btn-login-text" href="welcome.php">Accedi</a> </button>
        </form>

    </div>

    <?php include "../components/footer.php" ?>