<?php
// Initialize the session
session_start();
 
$_SESSION = array();
 
// distruggi sessione
session_destroy();
 
// rinderizza alla pagina di login
header("location: login.php");
exit;
?>