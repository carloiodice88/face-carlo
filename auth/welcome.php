<?php
// inizializza sessione
session_start();

//  Verifica se l'utenet è gia loggato altrimenti lo rimanda al login
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: ./login.php");
    exit;
}

?>
<?php include '../components/config.php' ?>


<?php $page='home'; include '../components/header-dashboard.php' ?>

<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <!-- Visite totali -->
                        <div class="col-md-4">
                            <div class="card ">
                                <div class="card-header ">
                                    <h4 class="card-title">VISITE TOTALI</h4>
                                </div>
                                <div class="card-body ">
                                    <H3>0</H3>
                                    <hr>
                                    <div class="stats">
                                        <i class="fa fa-clock-o"></i> Agiornato 2 ore fa
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Volti Rilevati -->
                        <div class="col-md-4">
                            <div class="card ">
                                <div class="card-header ">
                                    <h4 class="card-title">VISITE TOTALI</h4>
                                </div>
                                <div class="card-body ">
                                    <H3>100</H3>
                                    <hr>
                                    <div class="stats">
                                        <i class="fa fa-clock-o"></i> Agiornato 2 ore fa
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--  -->
                        <div class="col-md-4">
                            <div class="card ">
                                <div class="card-header ">
                                    <h4 class="card-title">VISITE TOTALI</h4>
                                </div>
                                <div class="card-body ">
                                    <H3>--</H3>
                                    <hr>
                                    <div class="stats">
                                        <i class="fa fa-clock-o"></i> Agiornato 2 ore fa
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Tabella-->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card strpied-tabled-with-hover">
                                    <div class="card-header ">
                                        <h4 class="card-title">VOLTI INSERITI</h4>
                                    </div>
                                    <div class="card-body table-full-width table-responsive">
                                        <table class="table table-hover table-striped">
                                            <thead>
                                                <th>ID</th>
                                                <th>Nome</th>
                                                <th>Cognome</th>
                                                <th>Visualizza</th>
                                                <th>Elimina</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>Carlo</td>
                                                    <td>Iodice</td>
                                                    <td><i class="fa fa-eye" aria-hidden="true"></i></td>
                                                    <td><i class="fa fa-trash" aria-hidden="true"></i></td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>Davide</td>
                                                    <td>Palladino</td>
                                                    <td><i class="fa fa-eye" aria-hidden="true"></i></td>
                                                    <td><i class="fa fa-trash" aria-hidden="true"></i></td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>Antonio</td>
                                                    <td>Schiavone</td>
                                                    <td><i class="fa fa-eye" aria-hidden="true"></i></td>
                                                    <td><i class="fa fa-trash" aria-hidden="true"></i></td>
                                                </tr>
                                                <tr>
                                                    <td>4</td>
                                                    <td>Lorena</td>
                                                    <td>Laurenza</td>
                                                    <td><i class="fa fa-eye" aria-hidden="true"></i></td>
                                                    <td><i class="fa fa-trash" aria-hidden="true"></i></td>
                                                </tr>
                                                <tr>
                                                    <td>5</td>
                                                    <td>Alfonso</td>
                                                    <td>Scuotto</td>
                                                    <td><i class="fa fa-eye" aria-hidden="true"></i></td>
                                                    <td><i class="fa fa-trash" aria-hidden="true"></i></td>
                                                </tr>
                                                <tr>
                                                    <td>6</td>
                                                    <td>Renato</td>
                                                    <td>Peluso</td>
                                                    <td><i class="fa fa-eye" aria-hidden="true"></i></td>
                                                    <td><i class="fa fa-trash" aria-hidden="true"></i></td>
                                                </tr>
                                                <tr>
                                                    <td>7</td>
                                                    <td>Pasquale</td>
                                                    <td>Ferraresi</td>
                                                    <td><i class="fa fa-eye" aria-hidden="true"></i></td>
                                                    <td><i class="fa fa-trash" aria-hidden="true"></i></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- Fine Tabella-->
                        </div>
                    </div>
                </div>
            </div>

<?php include '../components/footer-dashboard.php'?>

