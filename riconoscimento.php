<?php include "./components/config.php" ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Carlo</title>
    <link rel="stylesheet" href="css/styles.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

</head>

<header>

    <nav class="menu">
        <a class="brand-menu" href="#">Logo</a>
        <!--<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button> -->
        <div class="item">
            <ul class="menu-list">
                <li class="menu-item ">
                    <a href="https://rilface.000webhostapp.com/riconoscimento.php" class="menu-link ">Riconoscimento</a>
                </li>
                <li class="menu-item right ">
                    <a href="https://rilface.000webhostapp.com/auth/login.php" class="menu-link "><i class="fa fa-user "></i></a>
                </li>
            </ul>
        </div>

    </nav>

</header>

<body>
    <video id="video" height="480" width="1000" autoplay muted></video>
    <script src="js/face-api.min.js"></script>
    <script src="js/main.js"></script>
    <!--Script riconosciemnto-->
    <script>
        const video = document.getElementById("video");
        let predictedAges = [];

        Promise.all([
            faceapi.nets.tinyFaceDetector.loadFromUri("./models"),
            faceapi.nets.faceLandmark68Net.loadFromUri("./models"),
            faceapi.nets.faceRecognitionNet.loadFromUri("./models"),
            faceapi.nets.faceExpressionNet.loadFromUri("./models"),
            faceapi.nets.ageGenderNet.loadFromUri("./models"),
            faceapi.nets.ssdMobilenetv1.loadFromUri('./models')
        ]).then(startVideo);

        async function startVideo() {
            navigator.getUserMedia({
                    video: {}
                },
                stream => (video.srcObject = stream),
                err => console.error(err)
            );



            video.addEventListener("playing", async () => {
                const canvas = faceapi.createCanvasFromMedia(video);
                document.body.append(canvas);

                const labeledFaceDescriptors = await loadLabeledImages()
                const faceMatcher = await new faceapi.FaceMatcher(labeledFaceDescriptors, 0.6);

                const displaySize = {
                    width: video.width,
                    height: video.height
                };
                faceapi.matchDimensions(canvas, displaySize);

                setInterval(async () => {
                    const detections = await faceapi
                        .detectAllFaces(video, new faceapi.TinyFaceDetectorOptions())
                        .withFaceLandmarks()
                        .withFaceExpressions()
                        .withAgeAndGender()
                        .withFaceDescriptors();

                    if (!detections.length) {
                        return
                    }
                    const resizedDetections = faceapi.resizeResults(detections, displaySize);
                    canvas.getContext("2d").clearRect(0, 0, canvas.width, canvas.height);

                    faceapi.draw.drawDetections(canvas, resizedDetections);
                    faceapi.draw.drawFaceLandmarks(canvas, resizedDetections);
                    faceapi.draw.drawFaceExpressions(canvas, resizedDetections);

                    if (typeof resizedDetections[0] !== 'undefined') {

                        const age = resizedDetections[0].age;
                        const interpolatedAge = interpolateAgePredictions(age);
                        const results = resizedDetections.map(d => faceMatcher.findBestMatch(d.descriptor))

                        const bottomRight = {
                            x: resizedDetections[0].detection.box.bottomRight.x - 50,
                            y: resizedDetections[0].detection.box.bottomRight.y
                        };
                        const botBottomRight = {
                            x: resizedDetections[0].detection.box.bottomRight.x - 50,
                            y: resizedDetections[0].detection.box.bottomRight.y + 23
                        };
                        const bikiniBottom = {
                            x: resizedDetections[0].detection.box.bottomRight.x - 50,
                            y: resizedDetections[0].detection.box.bottomRight.y + 46
                        };

                        //Rilevatore età
                        /*new faceapi.draw.DrawTextField(
                            [`${faceapi.utils.round(interpolatedAge, 0)} anni`],
                            bottomRight
                        ).draw(canvas);*/

                        new faceapi.draw.DrawTextField(
                            [`${detections[0].gender}`],
                            botBottomRight
                        ).draw(canvas);

                        new faceapi.draw.DrawTextField(
                            [`${results[0]._label}`],
                            bikiniBottom
                        ).draw(canvas);
                        results.forEach((result, i) => {
                            const box = resizedDetections[i].detection.box
                            const drawBox = new faceapi.draw.DrawBox(box, {
                                boxColor: "#fff",
                                lineWidth: 2
                            })
                            drawBox.draw(canvas)
                        })

                    }

                }, 100);
            });
        }


        function interpolateAgePredictions(age) {
            predictedAges = [age].concat(predictedAges).slice(0, 30);
            const avgPredictedAge =
                predictedAges.reduce((total, a) => total + a) / predictedAges.length;
            return avgPredictedAge;
        }

        function loadLabeledImages() {
            const labels = ['Davide Palladino', 'Lorena Laurenza', 'Pasquale Ferraresi', 'Carlo Iodice']
            return Promise.all(
                labels.map(async label => {
                    const descriptions = []
                    for (let i = 1; i <= 2; i++) {
                        const img = await faceapi.fetchImage(`./images/${label}/${i}.jpg`)
                        const detections = await faceapi.detectSingleFace(img).withFaceLandmarks().withFaceDescriptor()
                        descriptions.push(detections.descriptor)
                    }
                    return new faceapi.LabeledFaceDescriptors(label, descriptions)
                })
            )
        }
        console.log(canvas)
    </script>
</body>

</html>